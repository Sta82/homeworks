var num = 1;

function testimonials() {
    if(num + 1 > 5) {
        num = 0;
    }
    num++;

    $('#slide_testimonials p').fadeOut('fast', function() {

    });

    setTimeout(function() {
        $('#slide_testimonials p').eq(num - 1).fadeIn('fast');
    }, 700);


}

setInterval(function() {
    testimonials();
}, 5000);



function slickSlider() {
    $('.autoplay').slick({
      slidesToScroll: 1,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 4000,
    });
}

$(document).ready(function() {
    slickSlider();
});


   
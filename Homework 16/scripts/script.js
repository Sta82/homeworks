function yearValues() {
var yearObj = document.querySelector('#year');

console.log(yearObj);

for(var i = 2010; i >= 1900; i--) {
    var optionEl = document.createElement('option');
    optionEl.innerHTML = i;
    optionEl.value = i;
    yearObj.appendChild(optionEl);
}}

window.addEventListener('load', function() {
    yearValues();
})
